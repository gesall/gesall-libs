### About

Repository of external libraries used in Gesall project.

### License

All copyright of Gesall code reserved by authors. This project includes code from other sources under their respective licenses.
